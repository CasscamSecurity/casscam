# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from results.models import CassCamResults
from shop.models import Product

full = 1
half = 0.5
freq_never = "never"


class HomepageSitemap(Sitemap):
    priority = full
    changefreq = freq_never

    def items(self):
        return ['home']

    def location(self, page):
        return reverse(page)


class ContactUsSitemap(Sitemap):
    priority = half
    changefreq = freq_never

    def items(self):
        return ['contact']

    def location(self, page):
        return "/" + page + "/"


class AboutUsSitemap(Sitemap):
    priority = half
    changefreq = freq_never

    def items(self):
        return ['about']

    def location(self, page):
        return "/" + page + "/"


class ProfileSitemap(Sitemap):
    priority = half
    changefreq = freq_never

    def items(self):
        return ['profile']

    def location(self, page):
        return "/" + page + "/"


class CassCamResultsSitemap(Sitemap):
    priority = half
    changefreq = freq_never

    def items(self):
        return CassCamResults.objects.all()


class ProductsSitemap(Sitemap):
    priority = half
    changefreq = freq_never

    def items(self):
        return Product.objects.all()
