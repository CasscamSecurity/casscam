"""web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import (
    url,
    include
)
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic.base import TemplateView
from django.contrib.sitemaps.views import sitemap
from web.viewsets import RobotsView
from main.sitemap import (
    HomepageSitemap,
    ContactUsSitemap,
    AboutUsSitemap,
    ProfileSitemap,
    CassCamResultsSitemap,
    ProductsSitemap,
)

sitemaps = {
    'home': HomepageSitemap,
    'contact': ContactUsSitemap,
    'about': AboutUsSitemap,
    'profile': ProfileSitemap,
    'results': CassCamResultsSitemap,
    'products': ProductsSitemap,
}

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/seo/', include('seo.urls')),
    url(r'^api/web/', include('web.urls')),
    url(r'^api/results/', include('results.urls')),
    url(r'^api/timeline/', include('timeline.urls')),
    url(r'^api/shop/', include('shop.urls')),
    url(r'^$', TemplateView.as_view(template_name='main.html'), name='home'),
    url(r'^.*/$', TemplateView.as_view(template_name='main.html')),
    url(r'^robots\.txt', RobotsView, name='robots'),
    url(r'^sitemap\.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.simaps.views.sitemap'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
