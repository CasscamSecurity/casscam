# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from results.models import CassCamResults


class CassCamResultsAdmin(admin.ModelAdmin):
    list_filter = ['created_at', 'updated_at']
    readonly_fields = ('slug', 'meta_title', 'meta_description', )

admin.site.register(CassCamResults, CassCamResultsAdmin)
