# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import PermissionDenied
from utils.random_number import get_random_number


class PublishedManager(models.Manager):
    """ Manager that handles if user has permission or is object published """

    def get(self, *args, **kwargs):
        user = kwargs.pop('for_user', None)
        published_object = super(PublishedManager, self).get(*args, **kwargs)

        if (user and user.is_staff and user.is_active) or (published_object.is_published):
            return published_object
        return PermissionDenied

    def for_user(self, user):
        if user.is_staff and user.is_active:
            return super(PublishedManager, self).get_queryset()
        else:
            queryset = super(PublishedManager, self).get_queryset()
            return queryset.filter(models.Q(is_published=True))


class CanPublish(models.Model):
    """ Abstract class for publishing new cam results """
    created_at = models.DateTimeField(
        _('When is this result created.'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        _('When has this result been updated last'),
        auto_now=True
    )
    is_published = models.BooleanField(
        _('Will you publish this result?'),
        default=False
    )

    objects = PublishedManager()

    def is_visible(self):
        return self.is_published

    class Meta:
        abstract = True
        verbose_name = 'Can Publish'


class CassCamResults(CanPublish, models.Model):
    """ CassCam Results database model """
    title = models.CharField(
        _('Title:'),
        max_length=255
    )
    slug = models.SlugField(
        _('Slug field:'),
        max_length=255,
        unique=True,
        help_text=_('This field is automated by backend.')
    )
    description = models.CharField(
        _('Short result description:'),
        max_length=255,
        help_text=_('Use this field to shortly describe result.'),
    )
    result_in_weather_env = models.CharField(
        _('Result taken in weather environment:'),
        max_length=512,
        help_text=_('Where has this result been taken?')
    )
    result_content = models.TextField(
        _('Your results:'),
        help_text=_('Detailed result content.')
    )
    conclusion = models.CharField(
        _('Final result conclusion:'),
        max_length=512,
        help_text=_('Short result conclusion.')
    )
    meta_title = models.CharField(
        _('Meta title:'),
        max_length=255,
        help_text=_('This field is automated by backend, it is used for social sharing title.')
    )
    meta_description = models.TextField(
        _('Meta description:'),
        help_text=_('This field is automated by backend, it is used for social sharing description.')
    )

    def __unicode__(self):
        return str(self.title)

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return "/results/" + str(self.slug)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug = str(''.join(e for e in self.title if e.isalnum())).lower() + '-' + str(get_random_number())
        self.meta_title = self.title
        self.meta_description = self.description
        super(CassCamResults, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = "Cass Cam Result"
