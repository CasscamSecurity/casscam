# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from results.models import CassCamResults


class CassCamResultsSerializer(serializers.ModelSerializer):

    class Meta:
        model = CassCamResults
        fields = '__all__'
        read_only = ('meta_description', 'meta_title', )
