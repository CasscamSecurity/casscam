# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import (
    url,
    include
)
from rest_framework import routers
from results.viewsets import CassCamResultsViewSet

router = routers.DefaultRouter()
router.register(r'casscam', CassCamResultsViewSet, base_name='CassCamResults')

urlpatterns = [
    url(r'^', include(router.urls)),
]
