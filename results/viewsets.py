# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import (
    viewsets,
    permissions
)
from results.models import CassCamResults
from results.serializers import CassCamResultsSerializer


class CassCamResultsViewSet(viewsets.ReadOnlyModelViewSet):
    """ API Endpoint for Cass Cam results model """
    queryset = CassCamResults.objects.all()
    serializer_class = CassCamResultsSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    lookup_field = 'slug'

    def get_queryset(self):
        return CassCamResults.objects.for_user(self.request.user).order_by('-created_at')
