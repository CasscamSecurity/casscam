# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from seo.models import MetaTags


class MetaTagsAdmin(admin.ModelAdmin):
    pass

admin.site.register(MetaTags, MetaTagsAdmin)
