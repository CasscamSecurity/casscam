# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class MetaTags(models.Model):
    """ Database model for website meta tags"""
    title = models.CharField(max_length=255)
    description = models.TextField()
    page_name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Meta Tag"
        verbose_name_plural = "Meta Tags"
