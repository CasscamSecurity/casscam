# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from seo.models import MetaTags


class MetaTagsSerializer(serializers.ModelSerializer):

    class Meta:
        model = MetaTags
        fields = ('title', 'description', 'page_name', )
