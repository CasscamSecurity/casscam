from django.conf.urls import (
    url,
    include
)
from rest_framework import routers
from seo.viewsets import MetaTagsViewSet


router = routers.DefaultRouter()
router.register('meta_tags', MetaTagsViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
