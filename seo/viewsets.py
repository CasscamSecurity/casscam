# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import viewsets
from seo.models import MetaTags
from seo.serializers import MetaTagsSerializer


class MetaTagsViewSet(viewsets.ReadOnlyModelViewSet):
    """ API Endpoint for meta tags model """
    queryset = MetaTags.objects.all()
    serializer_class = MetaTagsSerializer
    lookup_field = 'page_name'
