# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from shop.models import (
    ProductCategory,
    ProductImage,
    Product,
    Order
)


class ProductImageAdmin(admin.ModelAdmin):
    pass


class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    readonly_fields = ('slug', )


class ProductObjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'category', 'price', 'stock', 'available', 'created_at', 'updated_at']
    list_filter = ['available', 'created_at', 'updated_at', 'category']
    list_editable = ['price', 'stock', 'available']
    readonly_fields = ('slug', )


class OrdersAdmin(admin.ModelAdmin):
    pass


admin.site.register(ProductCategory, ProductCategoryAdmin)
admin.site.register(Product, ProductObjectAdmin)
admin.site.register(Order, OrdersAdmin)
admin.site.register(ProductImage, ProductImageAdmin)
