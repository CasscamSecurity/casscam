# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from shop.models import Cart
from django.utils.deprecation import MiddlewareMixin
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

User = get_user_model()


class EnsureCartExists(MiddlewareMixin):

    @staticmethod
    def process_request(request):
        if not request.session:
            request.session.create()

        if request.session.session_key is not None:
            try:
                session = Session.objects.get(session_key=request.session.session_key)
                session_data = session.get_decoded()

                uid = session_data.get('_auth_user_id')
                if uid is not None:
                    user = User.objects.get(id=uid)

                    shopping_cart = Cart.objects.get(owner=user.id)
                    if not shopping_cart:
                        shopping_cart = Cart()
                        shopping_cart.owner = user
                        shopping_cart.save()
                        request.session['cart_id'] = shopping_cart.id
            except:
                pass
