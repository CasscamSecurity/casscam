# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session
from decimal import Decimal
from utils.random_number import get_random_number

User = get_user_model()
logger = logging.getLogger('shop')


class ProductCategory(models.Model):
    """ Database model for product category table """
    name = models.CharField(
        _('Category name for your Products'),
        max_length=255,
        db_index=True,
        help_text=_('Name different categories for your Products.')
    )
    slug = models.SlugField(
        _('Unique slug field for category'),
        max_length=255,
        db_index=True,
        unique=True,
        help_text=_('Unique slug field that will be used for url routing.')
    )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        slug_tmp = ''.join(e for e in self.name if e.isalnum())
        self.slug = str(slug_tmp)
        super(ProductCategory, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])

    class Meta:
        ordering = ('name', )
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class ProductImage(models.Model):
    image = models.ImageField(
        _('Products Image'),
        upload_to='products/%Y-%m-%d',
        blank=True,
        help_text=_('This will be image of product displayed on webpage.')
    )

    def __unicode__(self):
        return self.image.url

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Product Image'


class Product(models.Model):
    """ Database model for product table """
    category = models.ForeignKey(
        ProductCategory
    )
    name = models.CharField(
        _('Product Name'),
        max_length=255,
        db_index=True,
        help_text=_('Product name that will be displayed on webpage.')
    )
    slug = models.SlugField(
        _('Products Slug Field'),
        max_length=255,
        db_index=True,
        help_text=_('Slug field that will be used for url routing.')
    )
    images = models.ManyToManyField(
        ProductImage,
        help_text=_('This will be image of product displayed on webpage.')
    )
    description = models.TextField(
        _('Products Description'),
        blank=True,
        help_text=_('Describe this product for customers.')
    )
    price = models.DecimalField(
        _('Products Price'),
        max_digits=10,
        decimal_places=2,
        help_text=_('Your products price.')
    )
    stock = models.PositiveIntegerField(
        _('Products Stock'),
        help_text=_('How many products are available in stock.')
    )
    available = models.BooleanField(
        _('Is Product Available'),
        default=True, help_text=_('Defines if product is available to purchase.')
    )
    created_at = models.DateTimeField(
        _('Product is created at current time'),
        auto_now_add=True,
        help_text=_('Automatically sets product created at current time.')
    )
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        slug_tmp = ''.join(e for e in self.name if e.isalnum())
        self.slug = str(slug_tmp).lower() + "-" + str(get_random_number())
        super(Product, self).save(force_insert, force_update, using, update_fields)

    def get_absolute_url(self):
        return '/product/details/' + str(self.slug)

    class Meta:
        ordering = ('-created_at', )
        verbose_name = 'Product'


class Order(models.Model):
    """ Database model for order table """
    product = models.ForeignKey(
        Product
    )
    quantity = models.IntegerField()

    def update_stock(self):
        """ stock update should be only called after successful transaction """
        self.product.stock -= self.quantity
        if self.product.stock < 0:
            logger.critical('Attempting to purchase {quantity} of {product_name} '
                            'but only {stock_quantity} left in stock.'.format(
                             quantity=self.quantity,
                             product_name=self.product.name,
                             stock_quantity=self.product.stock + int(self.quantity)))
            self.product.stock = 0
        if self.product.stock == 0:
            self.product.available = False
        self.product.save()

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'


class Cart(models.Model):
    items = models.ManyToManyField(
        Order
    )
    finalized = models.BooleanField(
        default=False,
        help_text=_('If cart is finalized then transaction will commit.')
    )
    suspicious = models.BooleanField(
        default=False,
        help_text=_('Is this transaction suspicious?')
    )
    owner = models.ForeignKey(
        User
    )

    def _validate_item(self, item_number, ipn_data):
        try:
            item_data = {
                'product':  ipn_data['item_number' + str(item_number)],
                'quantity': ipn_data['quantity' + str(item_number)],
                'price': Decimal(ipn_data['price' + str(item_number)])
            }
        except KeyError:
            return False

        try:
            product = Product.objects.get(id=item_data['product_id'])
        except Product.DoesNotExist:
            return False

        if not self.items.filter(product=product).exists():
            return False

        order = self.items.all()[item_number - 1]

        if product.price != item_data['price'] and order.quantity != item_data['quantity']:
            return False

        return True

    @staticmethod
    def get_from_session(session=None, session_key=None):
        assert (session or session_key), 'You must pass either session or session_key to Cart.get_from_session'
        if session_key:
            session = Session.objects.get(session_key=session_key).get_decoded()
        return Cart.objects.get(id=session['cart_id'])

    def flag_as_suspicious(self):
        self.suspicious = True
        self.finalized = True
        self.save()

    def finalize(self):
        for order in self.items.all():
            order.update_stock()
        self.finalized = True
        self.save()

    class Meta:
        verbose_name = 'Cart'
