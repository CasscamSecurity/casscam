# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import permissions
from shop.models import Cart


class OrderPermission(permissions.BasePermission):
    """ Custom order permission to allow users to place order and delete it """

    def has_object_permission(self, request, view, obj):
        if request.method == 'POST' or request.method == 'DELETE':
            return True
        shopping_cart = Cart.objects.get(id=request.session['cart_id'])
        return obj in shopping_cart.items.all()
