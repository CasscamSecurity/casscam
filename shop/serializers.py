# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from shop.models import (
    ProductCategory,
    ProductImage,
    Product,
    Order,
    Cart
)


class ProductCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCategory
        fields = '__all__'


class ProductImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductImage
        fields = '__all__'


class ProductObjectSerializer(serializers.ModelSerializer):
    category = ProductCategorySerializer(many=False, read_only=True)
    out_of_stock = serializers.SerializerMethodField('is_out_of_stock')
    images = ProductImageSerializer(many=True, read_only=True)

    @staticmethod
    def is_out_of_stock(obj):
        return obj.stock <= 0

    class Meta:
        model = Product
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    product = ProductObjectSerializer(many=False, read_only=True)

    def validate(self, attrs):
        quantity = attrs['quantity']
        product = attrs['product']

        if quantity < 1:
            raise serializers.ValidationError("You must at least order 1 item.")
        if quantity > product.stock:
            raise serializers.ValidationError({
                'quantity': "We're sorry, it appears we do not have enough ordered items in stock"
            })
        return attrs

    class Meta:
        model = Order
        fields = ('id', 'product', 'quantity')


class CartSerializer(serializers.ModelSerializer):

    items = OrderSerializer(many=True, read_only=False)

    class Meta:
        model = Cart
        fields = ('id', 'items', 'owner')
