# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import routers
from django.conf.urls import (
    url,
    include
)
from shop.viewsets import (
    ProductCategoryViewSet,
    ProductImageViewSet,
    ProductViewSet,
    OrderViewSet,
    CartViewSet
)

router = routers.DefaultRouter()
router.register(r'product-category',
                ProductCategoryViewSet,
                base_name='product-category')
router.register(r'products',
                ProductViewSet,
                base_name='products')
router.register(r'orders',
                OrderViewSet,
                base_name='orders')
router.register(r'cart',
                CartViewSet,
                base_name='cart')
router.register(r'product-images',
                ProductImageViewSet,
                base_name='product-images')

urlpatterns = [
    url(r'^', include(router.urls)),
]
