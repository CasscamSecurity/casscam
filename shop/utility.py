# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session
from shop.models import Cart
from django.contrib.auth import get_user_model

User = get_user_model()


def _update_session_data(session, session_data):
    session_store = SessionStore(session_key=session.session_key)
    session.session_data = session_store.encode(session_data)
    session.save()


def reset_user_cart(session_key):
    """ Create new cart """
    session = Session.objects.get(session_key=session_key)
    session_data = session.get_decoded()
    uid = session_data.get('_auth_user_id')
    user = User.objects.get(id=uid)
    shopping_cart = Cart()
    shopping_cart.objects.filter(owner=user.id)
    for item in shopping_cart.items.all():
        item.delete()
    shopping_cart.items.clear()
    session_data['cart_id'] = shopping_cart.id
    _update_session_data(session, session_data)
