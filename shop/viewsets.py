# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import (
    viewsets,
    permissions,
    response,
    status
)
from shop.models import (
    ProductCategory,
    ProductImage,
    Product,
    Order,
    Cart
)
from shop.serializers import (
    ProductCategorySerializer,
    ProductImageSerializer,
    ProductObjectSerializer,
    OrderSerializer,
    CartSerializer
)
from shop.permissions import (
    OrderPermission
)
from django.contrib.sessions.models import Session
from django.contrib.auth import get_user_model

User = get_user_model()


class ProductCategoryViewSet(viewsets.ReadOnlyModelViewSet):
    """ API Endpoint for Product Category model """
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer
    permission_classes = [
        permissions.AllowAny,
    ]


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """ API Endpoint for Product model """
    queryset = Product.objects.all()
    serializer_class = ProductObjectSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    lookup_field = 'slug'


class OrderViewSet(viewsets.ModelViewSet):
    """ API Endpoint for Order model """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [
        OrderPermission
    ]

    def get_queryset(self):
        try:
            session = Session.objects.get(session_key=self.request.session.session_key)
            session_data = session.get_decoded()

            uid = session_data.get('_auth_user_id')
            user = User.objects.get(id=uid)
            shopping_cart = Cart.objects.get(owner=user.id)
            return shopping_cart.items.all()
        except:
            return

    def create(self, request, *args, **kwargs):
        quantity = request.data['quantity']
        product = request.data['product']
        sessionId = request.data['requested_by']

        session = Session.objects.get(session_key=sessionId)
        session_data = session.get_decoded()

        uid = session_data.get('_auth_user_id')
        user = User.objects.get(id=uid)
        get_product = Product.objects.get(id=product)

        try:
            shopping_cart = Cart.objects.get(id=request.session['cart_id'])
        except:
            shopping_cart = Cart.objects.get(owner=user.id)

        try:
            cart = Order.objects.get(product=product)

            if cart.product.id == product:
                cart.quantity += 1
                cart.save()
        except:
            shopping_cart.items.create(product=get_product, quantity=quantity)
        """
        serializer = OrderSerializer(data={'product': product,
                                           'quantity': quantity})

        if serializer.is_valid():
            self.perform_create(serializer)
            #print serializer.data['id']
            instance = Order.objects.get(id=serializer.data['id'])

            try:
                shopping_cart = Cart.objects.get(id=request.session['cart_id'])
            except:
                shopping_cart = Cart.objects.get(owner=user.id)

            if shopping_cart.items.filter(product=product).exists():
                order = shopping_cart.items.get(product=product)
                quantity += order.quantity

            if shopping_cart.items.filter(product=instance.product).exists():
                order = shopping_cart.items.get(product=instance.product)
                order.quantity += instance.quantity
                order.save()
                instance.delete()
            else:
                shopping_cart.items.add(instance)
                shopping_cart.save()
            """
        return response.Response(status=status.HTTP_201_CREATED)


class CartViewSet(viewsets.ModelViewSet):
    """ API Endpoint for Cart model """
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]
    lookup_field = 'owner'

    """def get_queryset(self):
        try:
            session = Session.objects.get(session_key=self.request.session.session_key)
            session_data = session.get_decoded()

            uid = session_data.get('_auth_user_id')
            user = User.objects.get(id=uid)

            shopping_cart = Cart.objects.filter(owner=user.id).first()
            return shopping_cart
        except:
            return
    """
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        for order in instance.items.all():
            order.delete()

        instance.items.clear()
        self.get_success_headers(request.data)
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class ProductImageViewSet(viewsets.ReadOnlyModelViewSet):
    """ API Endpoint for Products image model """
    queryset = ProductImage.objects.all()
    serializer_class = ProductImageSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
