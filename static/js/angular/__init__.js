/**
 * Created by einjel on 7/24/17.
 */
(function () {
    "use strict";

    var app = angular.module('app', ['ngCookies', 'ui.router', 'ngResource', 'ngRoute', 'ngAnimate', 'ngFileUpload', 'sn.skrollr']);

    app.run(run);

    run.$inject = ['$http', '$rootScope', '$timeout', '$anchorScroll', '$cookies', 'snSkrollr'];

    function run($http, $rootScope, $timeout, $anchorScroll, $cookies, snSkrollr) {
        $rootScope.skrollrInstance = snSkrollr;
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
        $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        $rootScope.$on("$locationChangeSuccess", function () {
            $timeout(function () {
                $anchorScroll();
            });
            if(!$cookies.get('sessionid')){
                $rootScope.user_logged_in = false;
            } else {
                $rootScope.user_logged_in = true;
            };
        });

        $rootScope.title = 'CassCam Security';
        $rootScope.description = 'Stay safe';
    }
})();