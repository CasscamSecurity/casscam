/**
 * Created by einjel on 8/21/17.
 */
(function () {
    'use strict';

    angular.module('app').directive('lightgallery', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    element.parent().lightGallery();
                }
            }
        };
    });
})();
