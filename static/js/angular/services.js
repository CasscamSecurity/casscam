/**
 * Created by einjel on 7/24/17.
 */
(function () {
    "use strict";

    angular.module('app').factory('metaTagsRes', metaTagsRes);

    metaTagsRes.$inject = ['$resource'];

    function metaTagsRes($resource) {
        return $resource('/api/seo/meta_tags/:page_name/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('StaticPagesRes', StaticPagesRes);

    StaticPagesRes.$inject = ['$resource'];

    function StaticPagesRes($resource) {
        return $resource('/api/web/static_page/:url/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('ContactUsRes', ContactUsRes);

    ContactUsRes.$inject = ['$resource'];

    function ContactUsRes($resource) {
        return $resource('/api/web/contact/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('HomepageContentRes', HomepageContentRes);

    HomepageContentRes.$inject = ['$resource'];

    function HomepageContentRes($resource) {
        return $resource('/api/web/homepage/:company_name/ ', {}, {
            'query': {
                method: 'GET',
                isArray: true,
                cache: true
            },
            'get': {
                method: 'GET',
                isArray: false,
                cache: true
            }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('RegistrationRes', RegistrationRes);

    RegistrationRes.$inject = ['$resource'];

    function RegistrationRes($resource) {
        return $resource('/api/web/auth/private/register/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('LoginRes', LoginRes);

    LoginRes.$inject = ['$cookies', '$http'];

    function LoginRes($cookies, $http) {
        var LoginRes = {
            login: login,
            isAuthenticated: isAuthenticated
        };

        function login(username, password) {
            return $http.post('/api/web/auth/private/login/', {
                username: username,
                password: password
            });
        }

        function isAuthenticated() {
            if(!$cookies.get('authenticatedAccount')) {
                return false;
            };
            return true;
        }

        return LoginRes;
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('LogoutRes', LogoutRes);

    LogoutRes.$inject = ['$http'];

    function LogoutRes($http) {
        var LogoutRes = {
            logout: logout
        };

        function logout() {
            return $http.post('/api/web/auth/private/logout/');
        }

        return LogoutRes;
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('UserInfoRes', UserInfoRes);

    UserInfoRes.$inject = ['$resource'];

    function UserInfoRes($resource) {
        return $resource('/api/web/auth/private/whoami/ ', {}, {
            'query': {
                isArray: false
            },
            'update': { method: 'PUT' }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('ActivationRes', ActivationRes);

    ActivationRes.$inject = ['$http'];

    function ActivationRes($http) {
        var ActivationRes = {
            activate: activate
        };

        function activate(uid, token) {
            return $http.post('/api/web/auth/private/activate/', {
                uid: uid,
                token: token
            });
        }

        return ActivationRes;
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('PasswordResetRes', PasswordResetRes);

    PasswordResetRes.$inject = ['$http'];

    function PasswordResetRes($http) {
        var PasswordResetRes = {
            resetPassword: resetPassword
        };

        function resetPassword(email) {
            return $http.post('/api/web/auth/private/password/reset/', {
                email: email
            });
        }

        return PasswordResetRes;
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('PasswordResetConfirmRes', PasswordResetConfirmRes);

    PasswordResetConfirmRes.$inject = ['$resource'];

    function PasswordResetConfirmRes($resource) {
        return $resource('/api/web/auth/private/password/reset/confirm/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('CassCamResultsRes', CassCamResultsRes);

    CassCamResultsRes.$inject = ['$resource'];

    function CassCamResultsRes($resource) {
        return $resource('/api/results/casscam/:slug/ ', {}, {
            'query': {
                isArray: true,
                cache: true
            },
            'get': {
                isArray: false,
                cache: true
            }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('ProductCategoryRes', ProductCategoryRes);

    ProductCategoryRes.$inject = ['$resource'];

    function ProductCategoryRes($resource) {
        return $resource('/api/shop/product-category/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('ProductRes', ProductRes);

    ProductRes.$inject = ['$resource'];

    function ProductRes($resource) {
        return $resource('/api/shop/products/:slug/ ', {}, {
            'query': {
                isArray: true,
                cache: true
            },
            'get': {
                isArray: false,
                cache: true
            }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('OrdersRes', OrdersRes);

    OrdersRes.$inject = ['$resource'];

    function OrdersRes($resource) {
        return $resource('/api/shop/orders/:id/ ', {}, {
            'query': {
                isArray: true,
                cache: true
            },
            'get': {
                isArray: false,
                cache: true
            },
            'update': {
                method: 'PATCH'
            }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('CartRes', CartRes);

    CartRes.$inject = ['$resource'];

    function CartRes($resource) {
        return $resource('/api/shop/cart/:owner/ ', {}, {
            'query': {
                isArray: true,
                cache: true
            },
            'get': {
                isArray: false,
                cache: true
            }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('ProductImagesRes', ProductImagesRes);

    ProductImagesRes.$inject = ['$resource'];

    function ProductImagesRes($resource) {
        return $resource('/api/shop/product-images/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('AboutUsContentRes', AboutUsContentRes);

    AboutUsContentRes.$inject = ['$resource'];

    function AboutUsContentRes($resource) {
        return $resource('/api/web/about-content/ ');
    }
})();

(function () {
    "use strict";

    angular.module('app').factory('TimeLineNewsRes', TimeLineNewsRes);

    TimeLineNewsRes.$inject = ['$resource'];

    function TimeLineNewsRes($resource) {
        return $resource('/api/timeline/timeline/:slug/ ', {}, {
            'query': {
                isArray: true,
                cache: true
            },
            'get': {
                isArray: false,
                cache: true
            }
        });
    }
})();
