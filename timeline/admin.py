# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from timeline.models import TimeLineNews


class TimelineNewsAdmin(admin.ModelAdmin):
    readonly_fields = ('meta_title', 'meta_description', 'slug')


admin.site.register(TimeLineNews, TimelineNewsAdmin)
