# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TimelineConfig(AppConfig):
    name = 'timeline'
    verbose_name = 'Timeline'
