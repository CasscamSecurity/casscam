# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from results.models import (
    PublishedManager,
    CanPublish
)
from django.utils.translation import ugettext_lazy as _
from utils.random_number import get_random_number


class TimeLineNews(CanPublish, models.Model):
    title = models.CharField(
        _('Title:'),
        max_length=255,
    )
    description = models.TextField(
        _('Product Description:')
    )
    performance = models.CharField(
        _('Product Performance:'),
        max_length=255,
    )
    update_log = models.TextField(
        _('Update Log:'),
        help_text=_("This is admins only. Don't display this field to frontend")
    )
    meta_title = models.CharField(
        _('Meta Title:'),
        max_length=255,
        blank=True,
        help_text=_('This field is automated by backend.')
    )
    meta_description = models.CharField(
        _('Meta Description'),
        max_length=255,
        blank=True,
        help_text=_('This field is automated by backend.')
    )
    slug = models.SlugField(
        _('Slug field:'),
        max_length=511,
        help_text=_('This field is automated by backend.')
    )

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return str(self.title)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.meta_description = self.description
        self.meta_title = self.title
        self.slug = str("".join(e for e in self.title if e.isalnum())).lower() + '-' + str(get_random_number())
        super(TimeLineNews, self).save(force_update, force_insert, using, update_fields)

    def get_absolute_url(self):
        return '/timeline/' + str(self.slug)

    class Meta:
        verbose_name_plural = 'Timeline News'
