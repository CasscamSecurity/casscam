# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from timeline.models import TimeLineNews


class TimeLineNewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TimeLineNews
        fields = '__all__'
