# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import (
    url,
    include
)
from rest_framework import routers
from timeline.viewsets import TimeLineNewsViewSet


router = routers.DefaultRouter()
router.register(
    r'timeline',
    TimeLineNewsViewSet,
    base_name='timeline'
)

urlpatterns = [
    url(r'^', include(router.urls)),
]
