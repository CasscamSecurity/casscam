# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import (
    viewsets,
    permissions
)
from timeline.models import TimeLineNews
from timeline.serializers import TimeLineNewsSerializer


class TimeLineNewsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TimeLineNews.objects.all()
    serializer_class = TimeLineNewsSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    lookup_field = 'slug'
