# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from web.models import (
    StaticPages,
    HomepageContent,
    ContactUs,
    user,
    AboutUSContent,
)


class UserAdmin(admin.ModelAdmin):
    pass


class StaticPagesAdmin(admin.ModelAdmin):
    pass


class HomepageContentAdmin(admin.ModelAdmin):
    readonly_fields = ['company_name', ]


class ContactUsAdmin(admin.ModelAdmin):
    pass


class AboutUsContentAdmin(admin.ModelAdmin):
    pass


admin.site.register(StaticPages, StaticPagesAdmin)
admin.site.register(ContactUs, ContactUsAdmin)
admin.site.register(user, UserAdmin)
admin.site.register(AboutUSContent, AboutUsContentAdmin)
