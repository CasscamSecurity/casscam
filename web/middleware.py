# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
import re
from django.shortcuts import render_to_response
from django.template import TemplateDoesNotExist
from django.utils.deprecation import MiddlewareMixin


class PrerenderMiddleware(MiddlewareMixin):

    logger = logging.getLogger('prerender')

    @staticmethod
    def is_bot(request):
        is_google = '_escaped_fragment_' in request.GET
        user_agent = request.META.get('HTTP_USER_AGENT', '')
        return is_google or re.search('GoogleBot|'
                                      'Bingbot|'
                                      'Slurp|'
                                      'YandexBot|'
                                      'Exabot|'
                                      'DuckDuckBot|'
                                      'baiduspider|'
                                      'twitterbot|'
                                      'facebookexternalhit/1.1|'
                                      'rogerbot|'
                                      'linkedinbot|'
                                      'embedly|'
                                      'quora link preview|'
                                      'showyoubot|'
                                      'outbrain|'
                                      'pinterest|'
                                      'i686', user_agent)

    def process_request(self, request):
        if self.is_bot(request):
            template = 'snapshots%sindex.html' % request.path
            try:
                return render_to_response(template)
            except TemplateDoesNotExist:
                self.logger.error('Snapshot template {template} from path {path} is not found.'.format(
                    template=template,
                    path=request.path
                ))
