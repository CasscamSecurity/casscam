# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager,
    AbstractUser
)
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from django.core.mail import send_mail
from django.utils import six
from django.conf import settings
from django_resized import ResizedImageField

number_validator = RegexValidator(r'^[0-9+]*$', 'Must be numbers only')
USER_IMG_CROP = ['middle', 'center']
USER_IMG_SIZE = [250, 250]


class StaticPages(models.Model):
    """ Database model for static website pages """
    title = models.CharField(
        _('Page Title'),
        max_length=255,
        help_text=_('This is gonna be used as page title.')
    )
    description = models.TextField(
        _('Page Description'),
        max_length=511,
        blank=True, help_text=_('This is gonna be used as page description.')
    )
    content = models.TextField(
        _('Page Content'),
        help_text=_('This is gonna be used as page content.')
    )
    image = models.ImageField(
        _('Page Image'),
        upload_to='static_pages/',
        null=True,
        blank=True,
        help_text=_('This image will be used in given page.')
    )
    url = models.CharField(
        _('Page Url'),
        max_length=255,
        help_text=_('This will designate page url.')
    )

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.url[0] == '/':
            self.url = self.url[:1]
        if self.url[-1] == '/':
            self.url = self.url[:-1]
        super(StaticPages, self).save(force_insert, force_update, using, update_fields)

    def get_absolute_url(self):
        return "/" + str(self.url)

    class Meta:
        verbose_name = 'Static Page'
        verbose_name_plural = 'Static Pages'


class ContactUs(models.Model):
    """ Database model for contact us page"""
    user_name = models.CharField(
        _('Users Name'),
        max_length=255,
        help_text=_('This is name of a user who sent you an email.')
    )
    user_email = models.CharField(
        _('Users Email'),
        max_length=255,
        help_text=_('This is email of user which contacted you.')
    )
    email_content = models.TextField()

    def __unicode__(self):
        return str(self.user_email)

    def __str__(self):
        return str(self.user_email)

    def get_absolute_url(self):
        return "/contact/"

    class Meta:
        verbose_name = 'Contact Us Email'
        verbose_name_plural = 'Contact Us Emails'


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        username = self.model.normalize_username(username)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, password, **extra_fields)


class user(AbstractUser):
    """ Database model for generic user """

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(r'^[\w.@+-]+$',
                                      _('Enter a valid username. '
                                        'This value may contain only letters, numbers '
                                        'and @/./+/-/_ characters.'), 'invalid'),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(
        _('first name'),
        max_length=30,
        blank=True,
        help_text=_('Users First Name.')
    )
    last_name = models.CharField(
        _('last name'),
        max_length=30,
        blank=True,
        help_text=_('Users Last Name.')
    )
    email = models.EmailField(
        _('email address'),
        unique=True,
        help_text=_('This represents users verificated email.')
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_activated = models.BooleanField(
        _('activated'),
        default=False,
        help_text=_(
            'Revolves around if user activates his account from email link in activation service.'
        )
    )
    address = models.CharField(
        _('Users address'),
        max_length=255,
        blank=True,
        help_text=_('This is used for users personal address.')
    )
    phone_number = models.CharField(
        _('Users Phone Number'),
        max_length=255,
        blank=True,
        validators=[number_validator], help_text=_('This represents users current phone number.')
    )
    image = models.ImageField(
        _('Users Profile Image'),
        upload_to='images/users/',
        help_text=_('This is used for users profile thumbnail.'),
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    def __unicode__(self):
        return self.username

    def __str__(self):
        return self.username

    def get_full_name(self):
        return str(self.first_name + ' ' + self.last_name)

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def create_auth_token(sender, instance=None, created=False, **kwargs):
        if created:
            Token.objects.create(user=instance)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"


class UserThumbnail(models.Model):
    """ Database model for temporary user thumbnail image """
    image = ResizedImageField(
        _('User thumbnail image'),
        upload_to='images/users/temp/',
        crop=USER_IMG_CROP,
        size=USER_IMG_SIZE,
        help_text=_('This is temporary storage for user thumbnail')
    )
    session = models.CharField(max_length=511)

    def __unicode__(self):
        return self.session

    def __str__(self):
        return self.session

    class Meta:
        verbose_name = 'User Thumbnail'


class HomepageContent(models.Model):
    """ Database model for homepage """
    company_name = models.CharField(
        _('Company Name'),
        max_length=255,
        help_text=_('This will be used to query out database for text inputs.'),
        default=_('CassCam')
    )
    first_heading = models.CharField(
        _('Homepage Heading I'),
        max_length=255,
        help_text=_('This will be used as heading text for first text slider.'),
        default=_('Sample Heading')
    )
    first_content = models.TextField(
        _('Homepage Content I'),
        help_text=_('This will be used as content text for first text slider.'),
        default=_('Sample Content')
    )
    second_heading = models.CharField(
        _('Homepage Heading II'),
        max_length=255,
        help_text=_('This will be used as heading text for second text slider.'),
        default=_('Sample Heading')
    )
    second_content = models.TextField(
        _('Homepage Content II'),
        help_text=_('This will be used as content text for second text slider.'),
        default=_('Sample Content')
    )
    third_heading = models.CharField(
        _('Homepage Heading III'),
        max_length=255,
        help_text=_('This will be used as heading text for third text slider.'),
        default=_('Sample Heading')
    )
    third_content = models.TextField(
        _('Homepage Content III'),
        help_text=_('This will be used as content text for third text slider.'),
        default=_('Sample Content')
    )
    fourth_heading = models.CharField(
        _('Homepage Heading IV'),
        max_length=255,
        help_text=_('This will be used as heading text for fourth text slider.'),
        default=_('Sample Heading')
    )
    fourth_content = models.TextField(
        _('Homepage Content IV'),
        help_text=_('This will be used as content text for fourth text slider.'),
        default=_('Sample Content')
    )
    fifth_heading = models.CharField(
        _('Homepage Heading V'),
        max_length=255,
        help_text=_('This will be used as heading text for fifth text slider.'),
        default=_('Sample Heading')
    )
    fifth_content = models.TextField(
        _('Homepage Content V'),
        help_text=_('This will be used as content text for fifth text slider.'),
        default=_('Sample Content')
    )

    def __unicode__(self):
        return str(self.company_name)

    def __str__(self):
        return str(self.company_name)

    class Meta:
        verbose_name = 'Homepage Content'


class AboutUSContent(models.Model):
    slide_1_title = models.CharField(
        _('Slide I Title'),
        max_length=256,
        blank=True,
        help_text=_('Add title for this slide.')
    )
    slide_1_desc = models.CharField(
        _('Slide I Description'),
        max_length=512,
        blank=True,
        help_text=_('Add description for this slide.')
    )
    slide_2_title = models.CharField(
        _('Slide II Title'),
        max_length=256,
        blank=True,
        help_text = _('Add title for this slide.')
    )
    slide_2_desc = models.CharField(
        _('Slide II Description'),
        max_length=512,
        blank=True,
        help_text=_('Add description for this slide.')
    )
    slide_3_title = models.CharField(
        _('Slide III Title'),
        max_length=256,
        blank=True,
        help_text=_('Add title for this slide.')
    )
    slide_3_desc = models.CharField(
        _('Slide III Description'),
        max_length=512,
        blank=True,
        help_text=_('Add description for this slide.')
    )
    slide_4_title = models.CharField(
        _('Slide IV Title'),
        max_length=256,
        blank=True,
        help_text=_('Add title for this slide.')
    )
    slide_4_desc = models.CharField(
        _('Slide IV Description'),
        max_length=512,
        blank=True,
        help_text=_('Add description for this slide.')
    )
    slide_5_title = models.CharField(
        _('Slide V Title'),
        max_length=256,
        blank=True,
        help_text=_('Add title for this slide.')
    )
    slide_5_desc = models.CharField(
        _('Slide V Description'),
        max_length=512,
        blank=True,
        help_text=_('Add description for this slide.')
    )
    slide_6_title = models.CharField(
        _('Slide VI Title'),
        max_length=256,
        blank=True,
        help_text=_('Add title for this slide.')
    )
    slide_6_desc = models.CharField(
        _('Slide VI Description'),
        max_length=512,
        blank=True,
        help_text=_('Add description for this slide.')
    )

    def __str__(self):
        return 'About Us Page content'

    def __unicode__(self):
        return 'About Us Page content'

    class Meta:
        verbose_name = 'About Us Content'
        verbose_name_plural = 'About Us Content'
