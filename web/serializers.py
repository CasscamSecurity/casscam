# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from utils.image_resizer import get_responsive_image_url
from web.models import (
    StaticPages,
    ContactUs,
    user,
    UserThumbnail,
    HomepageContent,
    AboutUSContent,
)


class StaticPagesSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField('get_image_url')

    def get_image_url(self, instance):
        if instance.image == '':
            return ''
        return get_responsive_image_url(instance.image,
                                        self.context['request'].GET.get('image_size', ''))

    class Meta:
        model = StaticPages
        fields = '__all__'


class ContactUsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContactUs
        fields = '__all__'


class UserThumbnailSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserThumbnail
        fields = ('id', 'image', 'session')


class HomepageContentSerializer(serializers.ModelSerializer):

    class Meta:
        model = HomepageContent
        fields = '__all__'


class AboutUsContentSerializer(serializers.ModelSerializer):

    class Meta:
        model = AboutUSContent
        fields = '__all__'
