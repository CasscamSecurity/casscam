# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.dispatch.dispatcher import receiver
from django.db.models.signals import (
    post_save,
    post_delete
)
from django.conf import settings
from django.core.mail import send_mail
from web.models import (
    UserThumbnail,
    user,
    ContactUs
)


@receiver(post_save, sender=UserThumbnail)
def thumb_delete(sender, instance, **kwargs):
    if user.objects.filter(image=instance.image).first():
        return
    storage, path = instance.image.storage, instance.image.path
    storage.delete(path)


@receiver(post_delete, sender=UserThumbnail)
def thumb_delete(sender, instance, **kwargs):
    if user.objects.filter(image=instance.image).first():
        return
    storage, path = instance.image.storage, instance.image.path
    storage.delete(path)


@receiver(post_save, sender=ContactUs)
def send_contact_email(sender, instance, **kwargs):
    send_mail(subject='CassCam Security - Contact Form',
              message=instance.email_content,
              from_email=instance.user_email,
              recipient_list=['casscamdevelopers@gmail.com', ])
