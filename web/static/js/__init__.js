/**
 * Created by einjel on 7/27/17.
 */
/*================================================================
           simplest back button in history of life
================================================================*/
function goBack() {
    window.history.back();
}
/*===============================================================
                     navigation events:
                   - switch mobile state
                   - switch pc state
                 + on scroll up/down events
================================================================*/
function navigationEvents(a, b, c, d, e, f, g, h) {
    /* Enable mobile navigation */
    if(a.width() < 768) {
        b.css({'display': 'none'});
        c.css({'display': 'none'});
        d.css({'display': 'block'});
        e.css({'left': a.width() - 175});

        if (a.scrollTop() < 50) {
            d.addClass('transparent');
        } else {
            d.removeClass('transparent');
        }
    } else {
        /* Enable PC navigation */
        b.removeAttr('style');
        c.removeAttr('style');
        d.removeAttr('style');
        if(a.scrollTop() < 50) {
            makeTransparentNavigation(f, g, h);
        } else {
            removeTransparentNavigation(f, g, h);
        }
    }
}

/*================================================================
        navigation transparency for PC/Laptop version
================================================================*/
function makeTransparentNavigation(a, b, c) {
    a.addClass('transparent');
    b.addClass('whiteLogo');
    c.addClass('whiteUser');
}

function removeTransparentNavigation(a, b, c) {
    a.removeClass('transparent');
    b.removeClass('whiteLogo');
    c.removeClass('whiteUser');
}


/*================================================================
                   mobile navigation button
                animation is still a bit buggy
================================================================*/
function hamburgerClickEvents(a, b) {
    a.click(function () {
        if(b.attr('aria-expanded') === 'true') {
            b.removeClass('open');
        }
    });
    $(b).click(function () {
        if(typeof b.attr('aria-expanded') === 'undefined') {
            b.addClass('open');
        }
        if(b.attr('aria-expanded') === 'true') {
            b.removeClass('open');
        }
        if(b.attr('aria-expanded') === 'false') {
            b.addClass('open');
        }
    });
}
/*================================================================
         setting dynamic elements height in about us page
================================================================*/
function setAboutUsPageElementsHeight(a, b, c, d) {
    for(var j = 0; j < b.length; j++) {
        b[j].style.height = d.height() + 'px';
    }
    for(var i = 0; i < c.length; i++) {
        c[i].style.top = a.height() / 1.75 - 150 + 'px';
    }
}
/*=================================================================
                parallax effect for about us page
                        moving two elements
=================================================================*/
function ParallaxEffect(a, b, c) {
    var currentScroll = c.scrollTop(),
        scrollTo = currentScroll * 0.5;
    if(scrollTo < 300) {
        a.css({
            'top': scrollTo + 'px'
        });
    }
    if(scrollTo < 350) {
        b.css({
            'top': scrollTo * 0.150 + 'px'
        })
    }
}
/*=================================================================
             parallax effect for mobile static page
                      moving one element
=================================================================*/
function ParallaxEffectMobile(a, b) {
    var currentScroll = b.scrollTop(),
        scrollTo = currentScroll * 0.750;
    if(scrollTo < 300) {
        a.css({
            'top': scrollTo + 'px'
        });
    }
}
/*=================================================================
             styling for fixed static page position
                    when element is fixed
=================================================================*/
function staticPageStylish(a, b, c, d, e) {
    a.parent().css({
        'margin-bottom': 0 - b
    });
    a.css({
        'width': ((d.width()) / 1.5) + 'px',
        'height': ((d.width() / e) / 1.5) + 'px',
        'top': (((d.width() / e) - ((d.width() / e) / 1.5)) / 2) + 30 + 'px'
    });
    c.css({
        'width': (d.width() / 1.5) - 40 + 'px',
        'height': ((d.width() / e) / 1.5) - 40 + 'px'
    });
}
/*=================================================================
                       auto resize input
=================================================================*/
function resizeInput() {
    $(this).attr('size', $(this).val().length);
}


