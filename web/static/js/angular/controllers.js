/**
 * Created by einjel on 7/24/17.
 */
(function () {
    "use strict";

    angular.module('app').controller('SeoCtrl', SeoCtrl);

    SeoCtrl.$inject = ['$scope', '$rootScope', '$location'];

    function SeoCtrl($scope, $rootScope, $location) {
        $scope.$on('metaTagsChanged', function (event, metaTags) {
            $rootScope.title = metaTags.title;
            $rootScope.description = metaTags.description;
            $rootScope.location = $location.path();
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('HomeAbstractCtrl', HomeAbstractCtrl);

    HomeAbstractCtrl.$inject = ['$rootScope', '$scope', '$location'];

    function HomeAbstractCtrl($rootScope, $scope, $location) {
        $scope.home = $location.path() === '/';
        $scope.about = $location.path() === '/about/';
        $scope.$on('$stateChangeSuccess', function () {
            $scope.home = $location.path() === '/';
            $scope.about = $location.path() === '/about/';
            var navColor = angular.element($('.transparent ul > li > a')),
                userColor = angular.element($('.userIcon'));
            if( !$scope.home && !$scope.about ) {
                navColor.addClass('black');
                userColor.addClass('black');
            } else {
                navColor.removeClass('black');
                userColor.removeClass('black');
            }
        });
        $rootScope.$on('$locationChangeSuccess', function () {
            if($scope.home) {
                // calling skrollr instance from rootScope in case current page is homepage
                $rootScope.skrollrInstance.init({
                    easing: {
                        sin: function(p) {
                            if(p < 0.195) {
                                console.log(p);
                                return (Math.sin(p * Math.PI * 3 - Math.PI / 1.3) + 0.7) / 2;
                            } else {
                                p = 0.195;
                                return (Math.sin(p * Math.PI * 3 - Math.PI / 1.3) + 0.7) / 2;
                            }
                        },
                        cos: function(p) {
                            if(p < 0.195) {
                                return (Math.cos(p * Math.PI * 2 - Math.PI / 2) + 1) / 2;
                            } else {
                                p = 0.195;
                                return (Math.cos(p * Math.PI * 2 - Math.PI / 2) + 1) / 2;
                            }
                        }
                    }
                });
            } else {
                // destroying skrollr instance in case current page is not homepage
                $rootScope.skrollrInstance.destroy();
            }
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('HomeContentCtrl', HomeContentCtrl);

    HomeContentCtrl.$inject = ['$scope', 'HomepageContentRes'];

    function HomeContentCtrl($scope, HomepageContentRes) {
        $scope.homepageResource = HomepageContentRes.get({
            company_name: 'CassCam'
        });
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', '$rootScope', 'metaTags'];

    function HomeCtrl($scope, $rootScope, metaTags) {
        "use strict";

        $scope.$emit('metaTagsChanged', metaTags);

        $rootScope.currentPage = 'home';
        $rootScope.isStaticPage = false;
        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('ContactUsCtrl', ContactUsCtrl);

    ContactUsCtrl.$inject = ['$scope', '$rootScope', 'metaTags', 'page', 'ContactUsRes'];

    function ContactUsCtrl($scope, $rootScope, metaTags, page, ContactUsRes) {
        $scope.$emit('metaTagsChanged', metaTags);
        $scope.contact_page_resource = page;
        $scope.contact_us = {};
        $rootScope.image = page.image;
        $scope.contact_sent = false;

        $rootScope.currentPage = 'contact';
        $rootScope.isStaticPage = true;

        function clearInputs() {
            $scope.contact_us = {};
        }

        $scope.closeInformation = function () {
            $scope.contact_sent = false;
        };

        $scope.submit = function () {
            ContactUsRes.save({
                'user_name': $scope.contact_us.name,
                'user_email': $scope.contact_us.email,
                'email_content': $scope.contact_us.content
            }, function (response) {
                $scope.contact_sent = true;
                clearInputs();
                $scope.data = response.data;
            }, function (response) {
                $scope.errors = response.data;
            });
        };
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('AboutUsCtrl', AboutUsCtrl);

    AboutUsCtrl.$inject = ['$scope', '$rootScope', 'metaTags', 'page', 'content'];

    function AboutUsCtrl($scope, $rootScope, metaTags, page, content) {
        $scope.$emit('metaTagsChanged', metaTags);
        $scope.about_page_content = content[0];
        $scope.about_page_resource = page;
        $rootScope.image = page.image;
        $rootScope.isStaticPage = false;

    }
})();

(function () {
    "use strict";

    angular.module('app').controller('ActivationCtrl', ActivationCtrl);

    ActivationCtrl.$inject = ['$scope', '$rootScope', '$state', 'params', 'metaTags', 'page', 'ActivationRes'];

    function ActivationCtrl($scope, $rootScope, $state, params, metaTags, page, ActivationRes) {
        $scope.$emit('metaTagsChanged', metaTags);
        $scope.activation_page_resource = page;
        $rootScope.image = page.image;

        $scope.activation_complete = false;
        $rootScope.isStaticPage = false;

        var uid = params.split('?')[0],
            token = params.split('?')[1];

        ActivationRes.activate(uid, token).then(activationSuccess, activationFailed);

        function activationSuccess(response) {
            $scope.activation_complete = true;
            $state.go('website.home', { reload: true });
        }

        function activationFailed(response) {
            $scope.errors = response.data;
        }

        $scope.closeInformation = function () {
            $scope.activation_complete = true;
        };
    }
})();

(function () {
    "use strict";
    
    angular.module('app').controller('PasswordResetConfirmCtrl', PasswordResetConfirmCtrl);
    
    PasswordResetConfirmCtrl.$inject = ['$scope', '$rootScope', '$state', 'params', 'metaTags', 'page', 'PasswordResetConfirmRes'];
    
    function PasswordResetConfirmCtrl($scope, $rootScope, $state, params, metaTags, page, PasswordResetConfirmRes) {
        $scope.$emit('metaTagsChanged', metaTags);
        $scope.password_reset_confirm_resource = page;
        $rootScope.image = page.image;

        $scope.req = {};
        $scope.reset_complete = false;
        $rootScope.isStaticPage = false;
        $scope.missmatchPassword = false;

        var uid = params.split('?')[0],
            token = params.split('?')[1];

        $scope.closeInformation = function () {
            $scope.reset_complete = false;
            $state.go('website.home', { reload: true });
        };

        $scope.confirmReset = function () {
            if($scope.missmatchPassword) {
                $scope.missmatchPassword = false
            }
            if($scope.req.password === "" && $scope.req.re_password === "") {
                $scope.missmatchPassword = true;
                $scope.missmatchErrors = "These two fields are required.";
            } else {
                if ($scope.req.password === $scope.req.re_password) {
                    var post_data = {
                        uid: uid,
                        token: token,
                        new_password: $scope.req.password,
                        re_password: $scope.req.re_password
                    };
                    PasswordResetConfirmRes.save(post_data,
                        function (response) {
                            $scope.reset_complete = true;
                            $state.go('website.home', {reload: true});
                        }, function (response) {
                            $scope.errors = response.data;
                        });
                } else {
                    var error = 'Passwords miss match.';
                    $scope.missmatchPassword = true;
                    $scope.missmatchErrors = error;
                }
            }
        };
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('NavigationCtrl', NavigationCtrl);

    NavigationCtrl.$inject = ['$scope', '$rootScope', '$cookies', 'UserInfoRes', 'LoginRes', 'LogoutRes',
    'RegistrationRes', 'PasswordResetRes', 'Upload'];

    function NavigationCtrl($scope, $rootScope, $cookies, UserInfoRes, LoginRes, LogoutRes,
    RegistrationRes, PasswordResetRes, Upload) {

        $scope.log = {};
        $scope.loginCanvasActive = false;
        $scope.showMsgCanvas = false;
        $scope.user_has_thumbnail = false;
        $scope.bad_credentials = false;

        $cookies.get('sessionid') ? $rootScope.user_logged_in = true : $rootScope.user_logged_in = false;
        if(typeof $scope.user_data === 'undefined' && $rootScope.user_logged_in){
            $scope.user_data = UserInfoRes.get();
        }

        $rootScope.$on("$locationChangeSuccess", function () {
            $cookies.get('sessionid') ? $rootScope.user_logged_in = true : $rootScope.user_logged_in = false;
            if(typeof $scope.user_data === 'undefined' && $rootScope.user_logged_in){
                $scope.user_data = UserInfoRes.get();
            }
        });

        $scope.page_1 = true;
        $scope.rightArrowText = 'Register';
        $scope.currentTab = 'Sign In';
        $scope.page_2 = false;
        $scope.page_3 = false;

        $scope.registration = {};
        $scope.check_password = "";
        $scope.registeration_complete = false;
        $scope.password_match = true;

        $scope.passwordReset = {};
        $scope.reset_sent = false;

        $scope.profileUpdated = false;

        function clearInputs() {
            $scope.log = {};
            $scope.registration = {};
            $scope.passwordReset = {};
            $scope.check_password = "";
        }

        var msgCanvasElement = angular.element($('#msgCanvas'));

        $scope.switch_value = function () {
            $scope.check_password = angular.element($('#confirm_password'))[0].value;
        };

        // User Canvas Activate/Deactivate
        var loginCanvas = angular.element($('#loginCanvas'));

        function showCanvas() {
            $scope.loginCanvasActive = true;
            loginCanvas.css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }

        function hideCanvas() {
            $scope.loginCanvasActive = false;
            loginCanvas.css({
                'opacity': '0',
                'visibility': 'hidden'
            });
            clearInputs();
        }

        $scope.showLoginCanvas = function () {
            $scope.loginCanvasActive ? hideCanvas() : showCanvas();
        };
        // End of User Canvas Activate/Deactivate

        // Canvas Left/Right Arrow click functions
        $scope.ClickedLeft = function () {
            if($scope.page_2) {
                $scope.page_1 = true;
                $scope.page_2 = false;
                $scope.rightArrowText = 'Register';
            }
            if($scope.page_3) {
                $scope.page_2 = true;
                $scope.page_3 = false;
                $scope.rightArrowText = 'Forgot Password';
                $scope.leftArrowText = 'Sign In';
            }
            setTabName();
        };

        $scope.ClickedRight = function () {
            if($scope.page_2) {
                $scope.page_3 = true;
                $scope.page_2 = false;
                $scope.leftArrowText = 'Register';
            }
            if($scope.page_1) {
                $scope.page_2 = true;
                $scope.rightArrowText = 'Forgot Password';
                $scope.leftArrowText = 'Sign In';
                $scope.page_1 = false;
            }
            setTabName();
        };

        function setTabName() {
            if($scope.page_1) {
                $scope.currentTab = 'Sign In';
            }
            if($scope.page_2) {
                $scope.currentTab = 'Register';
            }
            if($scope.page_3) {
                $scope.currentTab = 'Forgot Password';
            }
        }
        // End of Left/Right Arrow click functions

        // Simple navigation interface
        $scope.goToRegister = function () {
            $scope.page_2 = true;
            $scope.page_1 = false;
            $scope.page_3 = false;
            setTabName();
        };

        $scope.goToRecover = function () {
            $scope.page_3 = true;
            $scope.page_2 = false;
            $scope.page_1 = false;
            setTabName();
        };
        // End of simple navigation interface

        // User logging in function
        $scope.loginUser = function () {
            LoginRes.login($scope.log.username, $scope.log.password)
                .then(loginSuccessfull, loginFailed);

            function loginSuccessfull(response) {
                $rootScope.user_logged_in = true;
                var userInfo = UserInfoRes.get(),
                    username = response.data.user,
                    user_id = response.data.id,
                    expireTime = new Date();


                $scope.user_data = userInfo;
                $scope.currentTab = 'Signed In';

                expireTime.setDate(expireTime.getTime() + 1);
                $cookies.put('authenticatedAccount', user_id, {'expired': expireTime});
                clearInputs();
                $rootScope.$broadcast('userLoggedIn', function () {
                    console.log('logged in');
                });
            }

            function loginFailed(response) {
                $scope.bad_credentials = true;
                $scope.errors = response.data;
                clearInputs();
            }
        };
        // End of User logging in function

        // Use logging out function
        $scope.SignOut = function () {
            LogoutRes.logout().then(logoutSuccessfull, logoutFailed);

            function unAuthenticate() {
                delete $cookies.remove('authenticatedAccount');
                $cookies.remove('authenticatedAccount');
            }

            function logoutSuccessfull(response) {
                $rootScope.user_logged_in = false;
                $scope.currentTab = 'Sign In';
                $scope.page_1 = true;
                $scope.page_2 = false;
                $scope.page_3 = false;
                $scope.preview = null;
                unAuthenticate();
                hideCanvas();
                clearInputs();
                $rootScope.$broadcast('userLoggedOut', function () {
                    console.log('logged in');
                });
            }

            function logoutFailed(response) {
                $scope.errors = response.data;
                clearInputs();
            }
        };
        // End of user logging out function

        // Register account profile thumbnail upload function
        $scope.upload = function (file) {
            if(!file) {
                return;
            }
            Upload.upload({
                url: '/api/web/auth/private/user-thumbnail/',
                file: file
            }).success(function (data, status, headers, config) {
                $scope.registration.image = data.image;
                $scope.preview = data.image;
                $scope.errors = false;
            }).error(function (data, status, headers, config) {
                $scope.errors = data.errors;
            });
        };
        // End of Register account profile thumbnail upload function

        // Account registration function
        $scope.RegisterAccount = function () {
            if($scope.check_password !== $scope.registration.password) {
                $scope.password_match = false;
            } else {
                $scope.registration.image = $scope.preview;
                RegistrationRes.save($scope.registration, function (resource) {
                    $scope.registeration_complete = true;
                    hideCanvas();
                    showMsgCanvas();
                    clearInputs();
                }, function (resource) {
                    $scope.errors = resource.data;
                    clearInputs();
                });
            }
        };
        // End of Account registration function

        // Password Reset function
        $scope.resetPW = function () {
            PasswordResetRes.resetPassword($scope.passwordReset.email).then(resetSuccess, resetFailed);

            function resetSuccess(response) {
                $scope.reset_sent = true;
                hideCanvas();
                showMsgCanvas();
                clearInputs();
            }

            function resetFailed(response) {
                $scope.errors = response.data;
                clearInputs()
            }
        };
        // End of password  reset function

        // Message system canvas
        function showMsgCanvas() {
            $scope.showMsgCanvas = true;
            msgCanvasElement.css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }

        $scope.closeMsgCanvas = function () {
            $scope.showMsgCanvas = false;
            $scope.registeration_complete = false;
            $scope.reset_sent = false;
            $scope.profileUpdated = false;
            $scope.profileDeleted = false;
            msgCanvasElement.css({
                'opacity': '0',
                'visibility': 'hidden'
            });
            clearInputs();
        };

        // Updating user account logic
        $scope.updateProfile = function () {
            $scope.user_data.image = $scope.preview;
            UserInfoRes.update($scope.user_data, function (response) {
                $scope.profileUpdated = true;
                hideCanvas();
                showMsgCanvas();
            }, function (response) {
                $scope.errors = response.data;
            });
        };

        $scope.deleteProfile = function () {
            LogoutRes.logout();
            $cookies.remove('authenticatedAccount');
            $cookies.remove('token');
            UserInfoRes.delete({},
                function (resource) {
                    $rootScope.user_logged_in = false;
                    $scope.profileDeleted = true;
                    hideCanvas();
                    showMsgCanvas();
                }, function (response) {
                    $scope.errors = response.data;
            });
        };
        // End of updating user account logic
    }
})();

(function () {
    "use strict";

    angular.module('app').constant('RESULTS_PER_PAGE', 6)
        .controller('CassCamResultsAbstractCtrl', CassCamResultsAbstractCtrl);

    CassCamResultsAbstractCtrl.$inject = ['$scope', '$state', '$sce',
        'RESULTS_PER_PAGE', 'metaTags', 'pageResource', 'results'];

    function CassCamResultsAbstractCtrl($scope, $state, $sce, RESULTS_PER_PAGE, metaTags, pageResource, results) {
        $scope.$emit('metaTagsChanged', metaTags);

        $scope.results_page_resource = pageResource;

        angular.forEach(results, function (result) {
            result.result_content = $sce.trustAsHtml(result.result_content);
        });

        $scope.TOTAL_PAGES = Math.ceil(results.length / RESULTS_PER_PAGE);

        var pageChanging = $scope.$on('pageChanged', function (event, page) {
            $scope.page = page;
        });

        $scope.$on('$destroy', function () {
            pageChanging();
        });

        $scope.previousPage = function () {
            $scope.page--;
            $state.go('website.results.list', { page: $scope.page });
        };

        $scope.nextPage = function () {
            $scope.page++;
            $state.go('website.results.list', { page: $scope.page });
        };
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('CassCamResultsListCtrl', CassCamResultsListCtrl);

    CassCamResultsListCtrl.$inject = ['$rootScope', '$scope', 'results', 'page', 'RESULTS_PER_PAGE'];

    function CassCamResultsListCtrl($rootScope, $scope, results, page, RESULTS_PER_PAGE) {
        $scope.$emit('pageChanged', page);
        $scope.page = page;

        var startingResultsListIndex = (page - 1) * RESULTS_PER_PAGE,
            endingResultsListIndex = startingResultsListIndex + RESULTS_PER_PAGE;

        $scope.results = results.slice(startingResultsListIndex, endingResultsListIndex);

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('CassCamResultsDetailsCtrl', CassCamResultsDetailsCtrl);

    CassCamResultsDetailsCtrl.$inject = ['$rootScope', '$scope', '$sce', 'result'];

    function CassCamResultsDetailsCtrl($rootScope, $scope, $sce, result) {

        result.result_content = $sce.trustAsHtml(result.result_content);

        $scope.result_resource = result;

        $scope.$emit('metaTagsChanged', {
            title: result.meta_title,
            description: result.meta_description
        });

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').constant('PRODUCTS_PER_PAGE', 4)
        .controller('ProductsAbstractCtrl', ProductsAbstractCtrl);

    ProductsAbstractCtrl.$inject = ['$scope', '$sce', '$state', 'metaTags', 'products',
        'PRODUCTS_PER_PAGE', 'pageResource'];

    function ProductsAbstractCtrl($scope, $sce, $state, metaTags, products, PRODUCTS_PER_PAGE,
                                  pageResource) {
        $scope.$emit('metaTagsChanged', metaTags);

        $scope.preorder_page_resource = pageResource;

        angular.forEach(products, function (product) {
            product.description = $sce.trustAsHtml(product.description);
        });

        $scope.TOTAL_SHOP_PAGES = Math.ceil(products.length / PRODUCTS_PER_PAGE);

        var changingPage = $scope.$on('pageChanged', function (event, page) {
            $scope.page = page;
        });

        $scope.$on('$destroy', function () {
            changingPage();
        });

        $scope.previousPage = function () {
            $scope.page--;
            $state.go('website.products.list', { page: $scope.page });
        };

        $scope.nextPage = function () {
            $scope.page++;
            $state.go('website.products.list', { page: $scope.page });
        };
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('ProductsListCtrl', ProductsListCtrl);

    ProductsListCtrl.$inject = ['$scope', '$rootScope', 'page', 'products', 'categories', 'PRODUCTS_PER_PAGE'];

    function ProductsListCtrl($scope, $rootScope, page, products, categories, PRODUCTS_PER_PAGE) {
        $scope.$emit('pageChanged', page);
        $scope.page = page;

        $scope.products = products;

        var startingProductsIndex = (page - 1) * PRODUCTS_PER_PAGE,
            endingProductsIndex = startingProductsIndex + PRODUCTS_PER_PAGE;

        $scope.products.slice(startingProductsIndex, endingProductsIndex);

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('ProductDetailsCtrl', ProductDetailsCtrl);

    ProductDetailsCtrl.$inject = ['$rootScope', '$scope', '$cookies', 'product', 'orders', 'OrdersRes'];

    function ProductDetailsCtrl($rootScope, $scope, $cookies, product, orders, OrdersRes) {
        $scope.product = product;

        $scope.$emit('metaTagsChanged', {
            title: $scope.product.name,
            description: $scope.product.description
        });

        $scope.$on('userLoggedIn', function () {
            $scope.canUseCart = true;
        });

        $scope.$on('userLoggedOut', function () {
            $scope.canUseCart = false;
        });

        $scope.orders = orders;

        $scope.quantity = 1;

        $scope.canUpdateOrder = false;

        $rootScope.user_logged_in ? $scope.canUseCart = true : $scope.canUseCart = false;

        $scope.refreshOrders = function (orders) {
            $scope.orders = orders;
        };

        function openCanvas() {
            var canvas = $('#showAuthMSG');
            canvas.css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }

        function addedCanvas() {
            var canv = $('#addedToCart');
            canv.css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }

        $scope.closeCanvas = function() {
            var canvas = $('#showAuthMSG');
            canvas.css({
                'opacity': '0',
                'visibility': 'hidden'
            });
        };

        $scope.closeAdded = function () {
            var vnac = $('#addedToCart');
            vnac.css({
                'opacity': '0',
                'visibility': 'hidden'
            });
        };

        $scope.addToCart = function () {
            if($scope.canUseCart) {
                OrdersRes.save({
                    requested_by: $cookies.get('sessionid'),
                    product: $scope.product.id,
                    quantity: $scope.quantity
                }, function (response) {
                    $scope.order_id = response.id;
                    $scope.canUpdateOrder = true;
                    OrdersRes.query({}, function (orders) {
                        $scope.refreshOrders(orders);
                    });
                    addedCanvas();
                }, function (response) {
                    $scope.errors = response.data;
                });
            } else {
                openCanvas();
            }
        };

        $scope.orderUpdate = function () {
            OrdersRes.update({id: $scope.order_id}, {
                quantity: $scope.quantity
            });
        };

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('UserCartCtrl', UserCartCtrl);

    UserCartCtrl.$inject = ['$rootScope', '$scope', '$cookies', '$window', 'metaTags', 'pageResource', 'CartRes', 'OrdersRes', 'cart'];

    function UserCartCtrl($rootScope, $scope, $cookies, $window, metaTags, pageResource, CartRes, OrdersRes, cart) {
        $scope.$emit('metaTagsChanged', metaTags);

        $scope.cart_page_resources = pageResource;

        $scope.reloadCart = function () {
            $window.location.reload()
        };

        $scope.refreshCart = function() {
            console.log(CartRes.query());
            if($rootScope.user_logged_in) {
                $scope.canUseCart = true;
                $scope.carts = cart;
                $scope.my_cart = {};
                var userid = parseInt($cookies.get('authenticatedAccount'), 10);
                angular.forEach($scope.carts, function (value, index) {
                    if (value.owner === userid) {
                        $scope.my_cart = value;
                    }
                });
                angular.forEach($scope.my_cart.items, function (value, index) {
                    value.totalPrice = value.quantity * value.product.price;
                });
                getSessionKey();
            } else {
                $scope.canUseCart = false;
            }
        };

        function getSessionKey() {
            $scope.sessionKey = $cookies.get('sessionid');
        }

        $scope.changeTotalPrice = function(obj) {
            angular.forEach($scope.my_cart.items, function (value, index) {
                if(value.id === obj.id) {
                    value.totalPrice = obj.quantity * value.product.price;
                }
            });
        };

        $scope.deleteOrder = function (obj, obj_index) {
            OrdersRes.delete({id: obj.id}, function (response) {
                $scope.my_cart.items.splice(obj_index, 1);
            }, function (response) {
                $scope.errors = response.data;
            });
        };

        $scope.deleteCartItems = function () {
            CartRes.delete({owner: $scope.my_cart.owner});
        };

        $scope.refreshCart();

        function openCanvas() {
            var canvas = $('#showAuthMSG');
            canvas.css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }

        $scope.closeCanvas = function() {
            var canvas = $('#showAuthMSG');
            canvas.css({
                'opacity': '0',
                'visibility': 'hidden'
            });
        };

        $scope.$on('userLoggedIn', function () {
            $scope.canUseCart = true;
            getSessionKey();
            CartRes.query({}, function () {
                $scope.refreshCart();
            });
        });

        $scope.$on('userLoggedOut', function () {
            $scope.canUseCart = false;
            $scope.my_cart = {};
        });

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').constant('TimeLineNews_PER_PAGE', 6)
        .controller('TimeLineNewsAbstractCtrl', TimeLineNewsAbstractCtrl);

    TimeLineNewsAbstractCtrl.$inject = ['$scope', '$state', 'pageResource', 'metaTags', 'timelines', 'TimeLineNews_PER_PAGE'];

    function TimeLineNewsAbstractCtrl($scope, $state, pageResource, metaTags, timelines, TimeLineNews_PER_PAGE) {
        $scope.$emit('metaTagsChanged', metaTags);

        $scope.timeline_page_resource = pageResource;

        $scope.TOTAL_TIMELINE_PAGES = Math.ceil(timelines.length / TimeLineNews_PER_PAGE);

        var changingPage = $scope.$on('pageChanged', function (event, page) {
            $scope.page = page;
        });

        $scope.$on('$destroy', function () {
            changingPage();
        });

        $scope.previousPage = function () {
            $scope.page--;
            $state.go('website.timeline.list', { page: $scope.page });
        };

        $scope.nextPage = function () {
            $scope.page++;
            $state.go('website.timeline.list', { page: $scope.page });
        };
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('TimeLineNewsListCtrl', TimeLineNewsListCtrl);

    TimeLineNewsListCtrl.$inject = ['$scope', '$rootScope', 'timelines', 'TimeLineNews_PER_PAGE', 'page'];

    function TimeLineNewsListCtrl($scope, $rootScope, timelines, TimeLineNews_PER_PAGE, page) {
        $scope.$emit('pageChanged', page);

        $scope.page = page;

        $scope.timelines = timelines;

        var TIMELINESTARTINDEX = (page - 1) * TimeLineNews_PER_PAGE,
            TIMELINEENDINDEX = TIMELINESTARTINDEX + TimeLineNews_PER_PAGE;

        $scope.timelines.slice(TIMELINESTARTINDEX, TIMELINEENDINDEX);

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('TimeLineNewsDetailCtrl', TimeLineNewsDetailCtrl);

    TimeLineNewsDetailCtrl.$inject = ['$scope', '$rootScope', 'timeline'];

    function TimeLineNewsDetailCtrl($scope, $rootScope, timeline) {
        $scope.timeline_resource = timeline;

        $scope.$emit('metaTagsChanged', {
            title: $scope.timeline_resource.title,
            description: $scope.timeline_resource.description
        });

        $rootScope.image = '';
    }
})();

(function () {
    "use strict";

    angular.module('app').controller('CookieCtrl', CookieCtrl);

    CookieCtrl.$inject = ['$scope', '$rootScope', 'metaTags'];

    function CookieCtrl($scope, $rootScope, metaTags) {
        $scope.$emit('metaTagsChanged', metaTags);

        $rootScope.image = '';
    }
})();

(function () {
    'use strict';

    angular.module('app').controller('FooterCtrl', FooterCtrl);

    FooterCtrl.$inject = ['$scope', '$window'];

    function FooterCtrl($scope, $window) {
        $scope.openFB = function () {
            $window.open('http://www.facebook.com/Casscam-Security-137957376825978/', '_blank');
        };

        $scope.openIN = function () {
            $window.open('http://twitter.com/casscamsecurity', '_blank');
        };

        $scope.openLN = function () {
            $window.open("http://www.linkedin.com/company/casscamsecurity", '_blank');
        };
    }
})();
