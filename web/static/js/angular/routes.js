/**
 * Created by einjel on 7/24/17.
 */
(function () {
    "use strict";

    angular.module('app').config(routes);

    routes.$inject = ['$resourceProvider', '$routeProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', 'snSkrollrProvider'];

    function routes($resourceProvider, $routeProvider, $locationProvider, $stateProvider, $urlRouterProvider, snSkrollrProvider) {
        "use strict";
        snSkrollrProvider.config = { smoothScrolling: true};
        $resourceProvider.defaults.stripTrailingSlashes = false;
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $urlRouterProvider.otherwise("/");
        $stateProvider.state('website', {
            abstract: true,
            template: '<ui-view/>'
        }).state('website.home', {
            url: '/',
            controller: 'HomeCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'home'
                    }).$promise;
                }
            }
        }).state('website.contact_us', {
            url: '/contact/',
            templateUrl: '/static/templates/contact_us/contact_us.html',
            controller: 'ContactUsCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'contact'
                    }).$promise;
                },
                page: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'contact'
                    }).$promise;
                }
            }
        }).state('website.about_us', {
            url: '/about/',
            templateUrl: '/static/templates/about_us/about_us.html',
            controller: 'AboutUsCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'about'
                    }).$promise;
                },
                page: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'about'
                    }).$promise;
                },
                content: function (AboutUsContentRes) {
                    return AboutUsContentRes.query().$promise;
                }
            }
        }).state('website.activate', {
            url: '/account/activate/?params',
            templateUrl: '/static/templates/auth/activation.html',
            controller: 'ActivationCtrl',
            resolve: {
                params: function ($stateParams) {
                    return $stateParams.params;
                },
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'activate_account'
                    }).$promise;
                },
                page: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'account/activate'
                    }).$promise;
                }
            }
        }).state('website.password_reset_confirm', {
            url: '/account/password/reset/confirm/?params',
            templateUrl: '/static/templates/auth/password_reset_confirm.html',
            controller: 'PasswordResetConfirmCtrl',
            resolve: {
                params: function ($stateParams) {
                    return $stateParams.params;
                },
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'password_reset_confirm'
                    }).$promise;
                },
                page: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'account/password/reset/confirm'
                    }).$promise;
                }
            }
        }).state('website.results', {
            abstract: true,
            templateUrl: '/static/templates/results/results_abstract.html',
            controller: 'CassCamResultsAbstractCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'results'
                    }).$promise;
                },
                pageResource: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'results'
                    }).$promise;
                },
                results: function (CassCamResultsRes) {
                    return CassCamResultsRes.query().$promise;
                }
            }
        }).state('website.results.list', {
            url: '/results/:page/',
            controller: 'CassCamResultsListCtrl',
            templateUrl: '/static/templates/results/results_list.html',
            resolve: {
                page: function ($stateParams) {
                    return parseInt($stateParams.page);
                }
            }
        }).state('website.result_details', {
            url: '/results/details/:slug/',
            controller: 'CassCamResultsDetailsCtrl',
            templateUrl: '/static/templates/results/results_details.html',
            resolve: {
                result: function (CassCamResultsRes, $stateParams) {
                    return CassCamResultsRes.get({
                        slug: $stateParams.slug
                    }).$promise;
                }
            }
        }).state('website.products', {
            abstract: true,
            templateUrl: '/static/templates/shopping/products_abstract.html',
            controller: 'ProductsAbstractCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'preorder'
                    }).$promise;
                },
                pageResource: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'preorder'
                    }).$promise;
                },
                products: function (ProductRes) {
                    return ProductRes.query().$promise;
                }
            }
        }).state('website.products.list', {
            url: '/shop/:page/',
            templateUrl: '/static/templates/shopping/products_list.html',
            controller: 'ProductsListCtrl',
            resolve: {
                page: function ($stateParams) {
                    return parseInt($stateParams.page);
                },
                productImages: function (ProductImagesRes) {
                    return ProductImagesRes.query().$promise;
                },
                categories: function (ProductCategoryRes) {
                    return ProductCategoryRes.query().$promise;
                }
            }
        }).state('website.product_details', {
            url: '/product/details/:slug/',
            templateUrl: '/static/templates/shopping/product_details.html',
            controller: 'ProductDetailsCtrl',
            resolve: {
                product: function (ProductRes, $stateParams) {
                    return ProductRes.get({
                        slug: $stateParams.slug
                    }).$promise;
                },
                orders: function (OrdersRes) {
                    return OrdersRes.query().$promise;
                }
           }
        }).state('website.shopping_cart', {
            url: '/cart/',
            templateUrl: '/static/templates/shopping/shopping_cart.html',
            controller: 'UserCartCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'cart'
                    }).$promise;
                },
                pageResource: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'cart'
                    }).$promise;
                },
                cart: function (CartRes) {
                    return CartRes.query().$promise;
                }
            }
        }).state('website.timeline', {
            abstract: true,
            templateUrl: '/static/templates/timeline/timeline_abstract.html',
            controller: 'TimeLineNewsAbstractCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'timeline'
                    }).$promise;
                },
                pageResource: function (StaticPagesRes) {
                    return StaticPagesRes.get({
                        url: 'timeline'
                    }).$promise;
                },
                timelines: function (TimeLineNewsRes) {
                    return TimeLineNewsRes.query().$promise;
                }
            }
        }).state('website.timeline.list', {
            url: '/timeline/:page/',
            templateUrl: '/static/templates/timeline/timeline_list.html',
            controller: 'TimeLineNewsListCtrl',
            resolve: {
                page: function ($stateParams) {
                    return parseInt($stateParams.page);
                }
            }
        }).state('website.timeline_details', {
            url: '/timeline/details/:slug/',
            templateUrl: '/static/templates/timeline/timeline_details.html',
            controller: 'TimeLineNewsDetailCtrl',
            resolve: {
                timeline: function (TimeLineNewsRes, $stateParams) {
                    return TimeLineNewsRes.get({
                        slug: $stateParams.slug
                    }).$promise;
                }
            }
        }).state('website.cookie_policy', {
            url: '/policies/cookies/',
            templateUrl: '/static/templates/policies/cookies.html',
            controller: 'CookieCtrl',
            resolve: {
                metaTags: function (metaTagsRes) {
                    return metaTagsRes.get({
                        page_name: 'cookie_policy'
                    }).$promise;
                }
            }
        });
    }
})();
