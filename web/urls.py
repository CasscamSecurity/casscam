# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import routers
from django.conf.urls import (
    url,
    include
)
from web.viewsets import (
    StaticPagesViewSet,
    ContactUsViewSet,
    HomepageContentViewSet,
    LoginViewSet,
    LogoutViewSet,
    UserViewSet,
    UserThumbnailViewSet,
    AboutUSContentViewSet
)
from utils.djoser.views import (
    RegistrationView,
    ActivationView,
    SetPasswordView,
    PasswordResetView,
    PasswordResetConfirmView,
    RootView,
)
from django.contrib.auth import get_user_model

User = get_user_model()

router = routers.DefaultRouter()
router.register(r'homepage', HomepageContentViewSet)
router.register(r'about-content', AboutUSContentViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^contact/', ContactUsViewSet.as_view(), name='contact'),
    url(r'static_page/(?P<url>.*)/', StaticPagesViewSet.as_view()),
    url(r'^auth/private/register/$', RegistrationView.as_view(), name='register'),
    url(r'^auth/private/activate/$', ActivationView.as_view(), name='activate'),
    url(r'^auth/private/password/$', SetPasswordView.as_view(), name='set_password'),
    url(r'^auth/private/password/reset/$', PasswordResetView.as_view(), name='reset_password'),
    url(r'^auth/private/password/reset/confirm/$', PasswordResetConfirmView.as_view(), name='confirm_reset_password'),
    url(r'^auth/private/login/$', LoginViewSet.as_view(), name='login'),
    url(r'^auth/private/logout/$', LogoutViewSet.as_view(), name='logout'),
    url(r'^auth/private/whoami/$', UserViewSet.as_view(), name='user_profile'),
    url(r'^auth/private/user-thumbnail/', UserThumbnailViewSet.as_view({'post': 'create'}), name='user_thumbnail'),
]
