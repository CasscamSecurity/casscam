# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import (
    viewsets,
    generics,
    permissions,
    views,
    response,
    status,
    parsers,
)
from web.models import (
    StaticPages,
    ContactUs,
    HomepageContent,
    UserThumbnail,
    AboutUSContent,
)
from web.serializers import (
    StaticPagesSerializer,
    ContactUsSerializer,
    HomepageContentSerializer,
    UserThumbnailSerializer,
    AboutUsContentSerializer,
)
from django.contrib.auth import (
    logout,
    login,
    authenticate,
    get_user_model
)
from django.http import HttpResponse
from utils.djoser import serializers
from shop.models import Cart
from rest_framework.authtoken.models import Token

User = get_user_model()


def RobotsView(request):

    response_text = ""
    http_host = request.get_host()

    if http_host:
        response_text = "User-agent: *\nDisallow: /"

    return HttpResponse(response_text, content_type="text/plain")


class StaticPagesViewSet(generics.RetrieveAPIView):
    """ API Endpoint for static pages model """
    queryset = StaticPages.objects.all()
    serializer_class = StaticPagesSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    lookup_field = 'url'


class ContactUsViewSet(generics.CreateAPIView):
    """ API Endpoint for saving sent emails from contact us page """
    queryset = ContactUs.objects.all()
    serializer_class = ContactUsSerializer
    permission_classes = [
        permissions.AllowAny,
    ]


class HomepageContentViewSet(viewsets.ReadOnlyModelViewSet):
    """ API Endpoint for homepage model """
    queryset = HomepageContent.objects.all()
    serializer_class = HomepageContentSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    lookup_field = 'company_name'


class LoginViewSet(views.APIView):
    """ API Endpoint for login view """
    permission_classes = [
        permissions.AllowAny,
    ]

    def post(self, request, format=None):
        try:
            usernameae = request.data['username']
            passwordae = request.data['password']
        except:
            return response.Response({
                'status': 'Cookie Error.',
                'message': 'Please clear Browser cookies.'
            }, status=status.HTTP_400_BAD_REQUEST)

        account = authenticate(username=usernameae, password=passwordae)

        if account is not None:
            obj = User.objects.get(username=usernameae)
            login(request, account)
            request.session['cart_id'] = Cart.objects.get_or_create(owner=User.objects.get(username=usernameae))[0].id
            Token.objects.get_or_create(user=User.objects.get(username=usernameae))
            return response.Response({
                'user': usernameae,
                'id': obj.id
            }, status=status.HTTP_200_OK)
        return response.Response({
            'status': 'Unauthorized',
            'message': 'Bad credentials.'
        }, status=status.HTTP_400_BAD_REQUEST)


class LogoutViewSet(views.APIView):
    """ API Endpoint for logout view """
    permission_classes = [
        permissions.AllowAny,
    ]

    def post(self, request, format=None):
        #request.user.auth_token.delete()
        cart_obj = Cart.objects.get(owner=User.objects.get(username=request.user))
        for item in cart_obj.items.all():
            item.delete()
        cart_obj.items.clear()
        logout(request)

        return response.Response(status=status.HTTP_200_OK)


class UserViewSet(generics.RetrieveUpdateDestroyAPIView):
    """ API Endpoint for user model """
    model = User
    serializer_class = serializers.serializers_manager.get('user')
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]

    def get_object(self, *args, **kwargs):
        return self.request.user


class UserThumbnailViewSet(viewsets.ModelViewSet):
    queryset = UserThumbnail.objects.all()
    serializer_class = UserThumbnailSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    parser_classes = [
        parsers.MultiPartParser,
    ]

    def create(self, request, *args, **kwargs):
        if not request.session.exists(request.session.session_key):
            request.session.create()

        image = request.data['file']
        serializer = UserThumbnailSerializer(data={'image': image,
                                                   'session': request.session.session_key})

        if serializer.is_valid():
            old_session = UserThumbnail.objects.filter(session=request.session.session_key).first()
            self.perform_create(serializer)

            if old_session:
                self.perform_destroy(old_session)

            _headers = self.get_success_headers(serializer.data)

            return response.Response(data=serializer.data,
                                     status=status.HTTP_201_CREATED,
                                     headers=_headers)
        return response.Response(data=serializer.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class AboutUSContentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AboutUSContent.objects.all()
    serializer_class = AboutUsContentSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
